import React from 'react';
import Login from '../components/LoginComponent'
import ChatBoxComponent from '../components/chatboxComponent'
import {
    Route,
    Link,
    Switch
  } from 'react-router-dom'
const routes = ()=> (
      <Switch>
          <Route exact path="/" component={Login}/>
          <Route exact path="/chatbox" component={ChatBoxComponent}/>
      </Switch>

)
export default routes 